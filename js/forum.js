// Get the form and messages elements
var form = document.getElementById("forum-form");
var messages = document.getElementById("messages");

displayMessages();
// Add a submit event listener to the form
form.addEventListener("submit", function(event) {
    // Prevent the form from refreshing the page
    event.preventDefault();

    // Get the movie name, nickname and message from the form
    var movieName = document.getElementById("movieName").value;
    var nickname = document.getElementById("nickname").value;
    var message = document.getElementById("message").value;

    // Create a timestamp as the key for the message
    var timestamp = new Date().getTime();

    // Use the key-value pair to store the message in local storage
    localStorage.setItem(timestamp, JSON.stringify({ movieName: movieName, nickname: nickname, message: message }));

    // Clear the form fields
    document.getElementById("movieName").value = "";
    document.getElementById("nickname").value = "";
    document.getElementById("message").value = "";

    // Display the messages from local storage
    displayMessages();
});

function displayMessages() {
    // Clear the messages element
    messages.innerHTML = "";

    // Iterate through the messages in local storage
    for (var i = 0; i < localStorage.length; i++) {
        var key = localStorage.key(i);
        var message = JSON.parse(localStorage.getItem(key));

        // Create a new div element for each message
        var messageElement = document.createElement("div");
        messageElement.classList.add("message");
        messageElement.innerHTML = "<b>Film: " + message.movieName + "</b><br>" + "<b>Nickname: " + message.nickname + "</b><br>" + "<b>Treść: " + message.message + "</b>";

        // Append the message to the messages element
        messages.appendChild(messageElement);

        //odkomentowac jak chcemy wyczyscic forum
       // localStorage.clear();
    }

}
