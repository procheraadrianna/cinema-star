const signUp = e => {
    let fname = document.getElementById('fname').value,
        lname = document.getElementById('surname').value,
        email = document.getElementById('email').value,
        number = document.getElementById('number').value,
        nick = document.getElementById('nick').value,
        pwd = document.getElementById('pwd').value;

    let formData = JSON.parse(localStorage.getItem('formData')) || [];

    let exist = formData.length &&
        JSON.parse(localStorage.getItem('formData')).some(data =>
            data.fname.toLowerCase() == fname.toLowerCase() &&
            data.lname.toLowerCase() == lname.toLowerCase()
        );

    if(!exist){
        formData.push({ fname, lname, email, pwd,number,nick });
        localStorage.setItem('formData', JSON.stringify(formData));
        document.querySelector('form').reset();
        document.getElementById('fname').focus();
        alert("Konto utworzone!");
        window.location.href='/cinema-star/html/zaloguj.html';
    }
    else{
        alert("Znaleziono duplikat!\n Wygląda na to, że masz już utworzone konto!");
        window.location.href='/cinema-star/html/zaloguj.html';
    }
    e.preventDefault();
}

function signIn(e) {
    let email = document.getElementById('email').value, pwd = document.getElementById('pwd').value;
    let formData = JSON.parse(localStorage.getItem('formData')) || [];
    let exist = formData.length &&
        JSON.parse(localStorage.getItem('formData')).some(data => data.email.toLowerCase() == email && data.pwd.toLowerCase() == pwd);
    if(!exist){
        alert("Podano niewłaściwe dane. \n\n Spróbuj ponownie.");
    }else{
        alert("Zalogowano pomyślnie!");
        window.location.href='/cinema-star/html/konto.html';
        sessionStorage.setItem("sessionToken", String(email));
    }
    e.preventDefault();

}

function isUserSignedIn() {
    if(sessionStorage.getItem("sessionToken")){
        document.getElementById("konto").disabled = false;
        window.location.href='/cinema-star/html/konto.html';
    }else {
        alert("Nie jesteś zalogowany!");
        document.getElementById("konto").disabled = true;

    }
}

function logOut() {
    sessionStorage.removeItem("sessionToken");
    window.location.href = "/cinema-star/html/komunikat_wylogowanie.html";
    // localStorage.clear();
    // sessionStorage.clear();
}
